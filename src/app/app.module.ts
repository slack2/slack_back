import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthModule } from 'modules/auth/auth.module';

@Module({
	imports: [
		MongooseModule.forRoot('mongodb+srv://Admin:Admin@cluster0.2pmspxp.mongodb.net/?retryWrites=true&w=majority'),
		AuthModule,
	],
	controllers: [],
	providers: [],
	exports: [],
})
export class AppModule {}
