export class ResponseClass<T = unknown> {
	constructor(
		public statusCode: number,
		public message: string[],
		public data?: T,
	) {}
}
