export enum Cookies {
	'Access-token' = 'Access-token',
	'Refresh-token' = 'Refresh-token'
}
