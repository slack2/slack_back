import { Module } from '@nestjs/common';

import { SharedModule } from 'modules/shared/shared.module';
import { AuthController } from 'modules/auth/controllers';
import {
	AuthService,
	TokensService,
} from 'modules/auth/services';
import { AlreadyRegisteredGuard } from 'modules/auth/guards';

@Module({
	imports: [SharedModule],
	controllers: [AuthController],
	providers: [
		AuthService,
		TokensService,
		AlreadyRegisteredGuard,
	],
	exports: [],
})
export class AuthModule {}
