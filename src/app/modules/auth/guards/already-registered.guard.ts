import {
	BadRequestException,
	CanActivate,
	ExecutionContext,
	HttpStatus,
	Injectable,
	Scope,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Request } from "express";

import {
	AuthDocument,
	AuthModel,
} from "modules/shared/system/models";
import { CreateRegisterDTO } from "modules/auth/system/dto";

@Injectable({
	scope: Scope.DEFAULT,
})
export class AlreadyRegisteredGuard implements CanActivate {
	constructor(
		@InjectModel(AuthModel.name) private readonly authModel: Model<AuthDocument>,
	) {}

	public async canActivate(context: ExecutionContext): Promise<boolean> {
		const request: Request<any, any, CreateRegisterDTO> = context.switchToHttp().getRequest<Request>();
		const {body: {name}} = request;

		if (name) {
			if (await this.authModel.findOne({name})) {
				throw new BadRequestException({
					message: 'User_is_already_exists',
					data: false,
					statusCode: HttpStatus.BAD_REQUEST,
				});
			}
		}

		return true;
	}
}
