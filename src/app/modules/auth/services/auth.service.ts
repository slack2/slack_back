import {
	Injectable,
	Scope,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

import {
	AuthDocument,
	AuthModel,
	TokenDocument,
	TokenModel,
} from 'modules/shared/system';
import { CreateRegisterDTO } from 'modules/auth/system/dto';
import { TokensService } from 'modules/auth/services';

@Injectable({
	scope: Scope.DEFAULT,
})
export class AuthService {
	constructor(
		@InjectModel(AuthModel.name) private readonly authModel: Model<AuthDocument>,
		@InjectModel(TokenModel.name) private readonly tokenModel: Model<TokenDocument>,
		private readonly tokensService: TokensService,
	) {}

	public async register(data: CreateRegisterDTO): Promise<[TokenDocument, AuthDocument]> {
		const codedPassword: string = await bcrypt.hash(String(data.password), Number(process.env.PASSWORD_KEY_HASH));

		const payload: CreateRegisterDTO = {
			...data,
			password: codedPassword,
		};

		const generatedTokens = this.tokensService.generateTokens(payload);

		const [tokens, auth] = await Promise.all([
			this.tokenModel.create(generatedTokens),
			this.authModel.create(payload),
		]);

		auth.tokens = tokens._id;
		tokens.auth = auth._id;

		await Promise.all([
			auth.save(),
			tokens.save(),
		]);

		return [tokens, auth];
	}
}
