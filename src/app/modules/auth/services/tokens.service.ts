import {
	Injectable,
	Scope,
} from '@nestjs/common';
import * as jsonwebtoken from 'jsonwebtoken';

import { CreateRegisterDTO } from 'modules/auth/system/dto';

@Injectable({
	scope: Scope.DEFAULT,
})
export class TokensService {
	public generateTokens(payload: CreateRegisterDTO): {accessToken: string, refreshToken: string} {
		const accessToken: string = this.generateToken(payload, process.env.ACCESS_TOKEN_SECRET, 1 * 60 * 5);
		const refreshToken: string = this.generateToken(payload, process.env.REFRESH_TOKEN_SECRET, 1 * 60 * 60 * 24);
		return {accessToken, refreshToken};
	}

	protected generateToken(payload: CreateRegisterDTO, secretKey: string, expiresIn: number): string {
		return jsonwebtoken.sign(
			payload,
			secretKey,
			{
				expiresIn,
				algorithm: 'HS256',
			},
		);
	}
}
