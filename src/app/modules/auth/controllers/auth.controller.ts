import {
	Body,
	Controller,
	HttpStatus,
	Post,
	Res,
	UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { AuthService } from 'modules/auth/services/auth.service';
import { CreateRegisterDTO } from 'modules/auth/system/dto';
import { AlreadyRegisteredGuard } from 'modules/auth/guards';
import { Cookies } from 'app/system/enums';
import { ResponseClass } from 'app/system';

@Controller('auth')
export class AuthController {
	constructor(
		protected readonly authService: AuthService,
	) {}

	@Post('register')
	@UseGuards(AlreadyRegisteredGuard)
	public async register(
		@Res() res: Response,
		@Body() body: CreateRegisterDTO,
	) {
		const [{accessToken, refreshToken}, auth] = await this.authService.register(body);

		res
			.cookie(Cookies['Access-token'], accessToken, {
				httpOnly: true,
			})
			.cookie(Cookies['Refresh-token'], refreshToken, {
				httpOnly: true,
			})
			.send(new ResponseClass(
				HttpStatus.CREATED,
				['User_registered_successfully'],
				auth,
			))
			.end();
	}
}
