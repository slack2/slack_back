import { IsNotEmpty } from "class-validator";

export class CreateRegisterDTO {
	@IsNotEmpty()
	public name: string;

	@IsNotEmpty()
	public password: string;
}
