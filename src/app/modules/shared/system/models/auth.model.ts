import {
	Prop,
	Schema,
	SchemaFactory,
} from "@nestjs/mongoose";
import {
	Document,
	Types,
} from "mongoose";
import { TokenDocument } from "./token.model";

@Schema()
export class AuthModel {
	@Prop()
	public name: string;

	@Prop()
	public password: string;

	@Prop({
		type: Types.ObjectId,
		ref: 'TokenModel',
	})
	public tokens: TokenDocument;
}

export const AuthSchema = SchemaFactory.createForClass(AuthModel);

export type AuthDocument = AuthModel & Document;
