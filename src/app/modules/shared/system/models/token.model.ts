import {
	Prop,
	Schema,
	SchemaFactory,
} from "@nestjs/mongoose";
import {
	Document,
	Types,
} from "mongoose";
import { AuthDocument } from "./auth.model";

@Schema()
export class TokenModel {
	@Prop()
	public accessToken: string;

	@Prop()
	public refreshToken: string;

	@Prop({
		type: Types.ObjectId,
		ref: 'AuthModel',
	})
	public auth: AuthDocument;
}

export const TokenSchema = SchemaFactory.createForClass(TokenModel);

export type TokenDocument = TokenModel & Document;
