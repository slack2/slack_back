import {
	Injectable,
	Scope,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { DeleteResult } from "mongodb";

import {
	AuthDocument,
	AuthModel,
	TokenDocument,
	TokenModel,
} from "../system";

@Injectable({
	scope: Scope.DEFAULT,
})
export class HelperService {
	constructor(
		@InjectModel(AuthModel.name) private readonly authModel: Model<AuthDocument>,
		@InjectModel(TokenModel.name) private readonly tokenModel: Model<TokenDocument>,
	) {}

	public dropAll(): Promise<DeleteResult[]> {
		return Promise.all([
			this.authModel.deleteMany(),
			this.tokenModel.deleteMany(),
		]);
	}
}
