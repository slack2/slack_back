import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import {
	AuthModel,
	AuthSchema,
	TokenModel,
	TokenSchema,
} from 'modules/shared/system';
import { HelperService } from 'app/modules/shared/services';
import { HelperController } from 'app/modules/shared/controllers';

@Module({
	imports: [
		MongooseModule.forFeature([
			{
				name: AuthModel.name,
				schema: AuthSchema,
			},
			{
				name: TokenModel.name,
				schema: TokenSchema,
			},
		]),
	],
	controllers: [HelperController],
	providers: [HelperService],
	exports: [
		MongooseModule,
		HelperService,
	],
})
export class SharedModule {}
