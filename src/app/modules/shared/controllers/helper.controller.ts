import {
	Controller,
	Get,
} from "@nestjs/common";
import { DeleteResult } from 'mongodb';

import { HelperService } from "app/modules/shared/services";

@Controller({
	path: 'helper',
})
export class HelperController {
	constructor(
		protected readonly helperService: HelperService,
	) {}

	@Get('drop')
	public drop(): Promise<DeleteResult[]> {
		return this.helperService.dropAll();
	}
}
