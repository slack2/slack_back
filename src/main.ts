import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as dotenv from 'dotenv';

import { AppModule } from 'app/app.module';

async function bootstrap() {
	dotenv.config();
	const app = await NestFactory.create(AppModule);
	app.setGlobalPrefix('api');

	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
			whitelist: true,
		}),
	);

	await app.listen(3000);
}
bootstrap();
